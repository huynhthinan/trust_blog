<?php
/**
 * function prefix: xbc_ (xeory base child)
 */
//require_once get_stylesheet_directory() . '/functions/functions-assets.php';

function xbc_setup(){
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// 画像サイズの指定
    //add_image_size('recommend_thumb', 113, 70, true);
    //add_image_size('main_thumb', 718, 380, true);
    add_image_size('loop_thumb', 1200, 700, true);
    add_image_size('medium', 400, 250, true);
}
add_action( 'after_setup_theme', 'xbc_setup' );

/*外部ファイルの読み込み（必ず子テーマにcss or jsフォルダを作る）*/
function mnc_wp_enqueue_scripts() {
 wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css' );//cssファイルの場合
 wp_enqueue_script('pagetop', get_stylesheet_directory_uri() . '/js/pagetop.js' );//jsファイルの場合
 //wp_enqueue_style('float-cta', get_stylesheet_directory_uri() . '/float-cta/float-cta.css' );//下固定バナーcss
 //wp_enqueue_script('float-cta', get_stylesheet_directory_uri() . '/float-cta/float-cta.js' );//下固定バナーjs
}
add_action( 'wp_enqueue_scripts', 'mnc_wp_enqueue_scripts' );

//管理画面スタイルの反映
add_editor_style("admin/editor-style.css");

//タイトルタグ内に不要なディスクリ等が入らないようにする
if(!function_exists('mypace_custom_title')){
function mypace_custom_title( $title ){
    if (!is_feed()) {
        $title['tagline']='';
        $title['site']='';
    }

  return $title;
}
}
add_filter( 'document_title_parts', 'mypace_custom_title', 10, 2 ); //フィルターフックで処理を上書き

remove_filter('pre_user_description', 'wp_filter_kses');//セーブのタイミングでプロフィール情報のHTMLタグを削除する
add_filter('pre_user_description', 'wp_filter_post_kses');//プロフィール情報のHTMLタグを削除しないようにする


/*------ css、jsのバージョン情報削除 ------*/
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

//contactform7
add_action( 'wp_footer', 'bzbsk_cf7_form_submit' );
function bzbsk_cf7_form_submit() {
    global $post;
    $slug = $post->post_name;
    ?>
    <script type="text/javascript">
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            window.location.href = '<?php echo get_home_url(); ?>/thanks?from=<?php echo $slug;?>';
        }, false );
    </script>
    <?php
}

/*--------------------------------------------------------------------------
    自作関数エリア
--------------------------------------------------------------------------*/

/*------ 下固定バナー ------*/
function bzbsk_float_cta() {
    get_template_part( 'float-cta/float', 'cta' );
}


/*------ パンくず表記変更 ------*/
function bzbsk_breadcrumbs() {
    ob_start();//バッファリング
    xeory_breadcrumbs();
    $bc = ob_get_clean();

    echo preg_replace('/HOME/', 'ホーム', $bc, 1);
}


/*------ 検索フォーム（add_filter用） ------*/
add_filter( 'get_search_form', 'bzbsk_custom_searchform' );
function bzbsk_custom_searchform( $form ) {
    $form = "<form role=\"search\" method=\"get\" class=\"search-form\" action=\"" . home_url() . "\">
                <label>
                    <span class=\"screen-reader-text\">検索:</span>
                    <input type=\"search\" class=\"search-field\" placeholder=\"フリーワード検索\" value=\"\" name=\"s\" />
                </label>
                <input type=\"submit\" class=\"search-submit\" value=\"&#xf002;\" />
            </form>\n";
    return $form;
}


/*------ 検索フォーム（add_action用） ------*/
function bzbsk_searchform() {
    echo '<div class="search-wrap">';
    get_search_form( true );
    echo '</div>';
}


/*------ loopで表示するサムネイル ------*/
function bzbsk_loop_thumb_w_cat() {
    global $post;//関数外にある変数$postを関数内で使えるようにする

    $thumbnail = get_the_post_thumbnail( $post->ID, $size = 'loop_thumb');

    $category = get_the_category();//カテゴリの配列
    //その投稿が属しているカテゴリーの数だけループさせる
    $name = "";
    foreach( $category as $cat ) {
        $name.=  $cat -> cat_name.', ';//カテゴリ名取得
    }
    $cat_name = rtrim($name, ', ');//最後のカテゴリ名後のカンマを削除

    echo "<figure class=\"entry-thumbnail\">";

    if ( ! is_single() ) {
        echo "<a href=\"".get_the_permalink( $post->ID )."\">" . $thumbnail . "</a>";
        echo "<p class=\"cat\">".$cat_name."</p>";
    } else {
        echo "<a href=\"".get_the_permalink( $post->ID )."\">".the_post_thumbnail('full')."</a>";
    }
    echo "</figure>";
}


/*------ loopで表示するコンテンツを抜粋表示 ------*/
function bzbsk_loop_content() {
    global $post;//関数外にある変数$postを関数内で使えるようにする

    $moji = 50;//表示したい文字数（変更可）
    $p_substr = mb_substr(strip_tags($post -> post_content), 0, $moji);
    $more = "<a href=\"".get_permalink($post -> ID)."\">続きを読む</a>";

    $date = get_the_time("Y/n/j");//日付表示はY年n月j日のように変更可

    //出力するHTMLソースは書き換え可
    echo "<p class=\"cont-str\">".$p_substr."…</p>
    <ul class=\"entry-meta\">
    <li class=\"date\"><span><i class=\"fa fa-clock-o\"></i></span>&nbsp;".$date."</li>
    <li class=\"more\">".$more."</li>\n</ul>";
}

/*------ loopで表示するカテゴリー、日付 ------*/
function bzbsk_cat_w_date() {
    $category = get_the_category();//カテゴリの配列
    $date = get_the_time("Y.n.j");//日付表示はY年n月j日のように変更可
    $modified = get_the_modified_date("Y.n.j");//更新日
    $length = count($category);
    $num = 0;

    echo "<ul class=\"entry-meta\">
            <li class=\"cat\">";

    //その投稿が属しているカテゴリーの数だけループさせる
    foreach( $category as $cat ) {
        echo '<a href="' . get_category_link( $cat->cat_ID ) . '">' . $cat->name . '</a>';
        $num++;

        if( $num !== $length ) {
            echo ', ';
        }
    }

    echo "</li>
        <li class=\"date\">" . $date . "</li>\n";

        if( $modified !== $date ) {
            echo "<li class=\"modified\">更新日：" . $modified . "</li>\n";
        }

    echo "</ul>";
}

/*------ コピーライトの出力 ------*/
function bzbsk_copyright() {
    $blog_name = get_bloginfo('name');
    echo '<p>All Rights Reserved,Copyright（C）2017<span>'.$blog_name.'</span>Co.,Ltd.</p>';
}


/*--------------------------------------------------------------------------
    remove・addエリア

    デフォルトで入っている関数リスト
    remove_action('xeory_prepend_entry-content', 'xeory_add_post_thumbnail');//アイキャッチ
    remove_action( 'xeory_prepend_container', 'xeory_breadcrumbs' );//パンくず表記変更前
    remove_action('xeory_prepend_entry-header', 'xeory_base_posted_on');//日付
    remove_action( 'xeory_after_entry', array( Xeory_Core::$xeory_users, 'bzb_show_avatar' ), 10 );//著者情報
--------------------------------------------------------------------------*/

/*------ after_all関数を最後に読み込む指示 ------*/
add_action( 'template_redirect', 'after_all' );

/*------ 一番最後に読み込む関数 ------*/
function after_all() {

    //ここにadd・removeを記述
    //add_action( 'xeory_after_site-footer', 'bzbsk_float_cta' );//下固定バナー

    //トップ
    remove_action('xeory_prepend_entry-content', 'xeory_add_post_thumbnail');//アイキャッチ
    remove_action( 'xeory_prepend_entry-header', 'xeory_base_posted_on' );//日付


    //検索結果・カテゴリーなし
    add_action('content_none_search', 'bzbsk_searchform');//検索窓

    //コピーライト
    add_action('xeory_append_site-info', 'bzbsk_copyright');//検索窓


    /*------ トップページ、カテゴリ、検索結果の時 ------*/
    if(is_home() || is_front_page() || is_archive() || is_search()) {

        //ここにadd・removeを記述
        add_action('xeory_prepend_entry-header', 'bzbsk_loop_thumb_w_cat');//アイキャッチ
        add_action('xeory_append_entry-content', 'bzbsk_loop_content');//本文抜粋
    }

    /*------ 投稿ページの時 ------*/
    if(is_single()) {
        //ここにadd・removeを記述
        add_action('xeory_prepend_entry-header', 'bzbsk_cat_w_date');//カテゴリーと日付
        add_action('xeory_prepend_entry-content', 'bzbsk_loop_thumb_w_cat');//アイキャッチ

        //SNSボタンをアイキャッチ下に移動
        remove_action('xeory_after_entry-header', array( Xeory_Core::$xeory_social->model_sb, 'model_social_buttons'));
        add_action('xeory_prepend_entry-content', array( Xeory_Core::$xeory_social->model_sb, 'model_social_buttons'));

         //著者情報
        remove_action( 'xeory_after_entry', array( Xeory_Core::$xeory_users, 'bzb_show_avatar' ), 10 );
        add_action( 'xeory_prepend_underpost-widget', array( Xeory_Core::$xeory_users, 'bzb_show_avatar' ), 10 );
    }

    /*------ 固定ページの時 ------*/
    if (is_page()){

        //記事上のSNSボタン削除
        remove_action('xeory_after_entry-header', array( Xeory_Core::$xeory_social->model_sb, 'model_social_buttons'));

        //記事下のSNSボタン削除
        remove_action('xeory_prepend_entry-footer', array( Xeory_Core::$xeory_social->model_sb, 'model_social_buttons'));
    }
}
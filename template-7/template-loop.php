<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Xeory-base
 */

?>



	<?php do_action( 'xeory_prepend_content-area' ); ?>

		<?php do_action( 'xeory_prepend_site-main' ); ?>

	<?php
	if ( have_posts() ) :

	while ( have_posts() ) : the_post(); $num++;

		//1ページ目の場合のみ、最初の記事を大きく表示する
		if(!is_paged() && is_home() || !is_paged() && is_front_page()) {//1ページ目だったら

			if($num == 1) {//1記事目だったら
				$cont_wrap_start = "<div class=\"post-loop-first\">";
				$cont_wrap_end = "";
			}

			elseif($num == 3) {//3記事目だったら
				$cont_wrap_start = "</div><div id=\"primary\" class=\"content-area\"><main id=\"main\" class=\"site-main\" role=\"main\"><div class=\"post-loop-wrap\">";
				$cont_wrap_end = "";
			}

			else {
				$cont_wrap_start = "";
				$cont_wrap_end = "";
			}
		}

		//2ページ目以降だったら
		else {
			if ( is_archive() ) {
					$string_header = '<header class="page-header">'.
						get_the_archive_title( '<h1 class="page-title">', '</h1>' ).
						get_the_archive_description( '<div class="taxonomy-description">', '</div>' ).
					'</header>';
			} else if ( is_search() ) {
				$string_header ='<header class="page-header"><h1 class="page-title">' . sprintf( esc_html__( 'Search Results for: %s', 'xeory-base' ), '<span>' . get_search_query() . '</span>' ) . '</h1></header>';
			}
			$cont_wrap_start = ($num == 1) ? '<div id="primary" class="content-area"><main id="main" class="site-main" role="main">'.$string_header.'<div class="post-loop-wrap">' : '';
			$cont_wrap_end = "";
		}

		echo $cont_wrap_start;

		do_action( 'xeory_before_entry' );
	?>

	<div class="article-list">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<?php do_action( 'xeory_prepend_entry' ); ?>

			<header class="entry-header">

				<?php do_action( 'xeory_prepend_entry-header' ); ?>

				<?php do_action( 'xeory_append_entry-header' ); ?>

			</header><!-- .entry-header -->

			<?php do_action( 'xeory_after_entry-header' ); ?>

			<div class="entry-content">

				<?php do_action( 'xeory_prepend_entry-content' ); ?>

				<?php
					if ( is_single() ) {
						the_title( '<h1 class="entry-title">', '</h1>' );
					} else {
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					}
				?>

				<?php do_action( 'xeory_append_entry-content' ); ?>

			</div><!-- .entry-content -->

			<?php do_action( 'xeory_after_entry-content' ); ?>

			<?php
				if ( is_single() ) {
					?>
			<footer class="entry-footer">

				<?php do_action( 'xeory_prepend_entry-footer' ); ?>
				<?php xeory_base_entry_footer(); ?>
				<?php do_action( 'xeory_append_entry-footer' ); ?>

			</footer><!-- .entry-footer -->

					<?php
				}
			?>

			<?php do_action( 'xeory_append_entry' ); ?>

		</article><!-- #post-## -->
	</div><!-- /article-list-->

	<?php do_action( 'xeory_after_entry' );

	echo $cont_wrap_end;

	endwhile;

	?>

	</div>

	<?php pagination($wp_query->max_num_pages);

		else :

	?>

	<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

	<?php if(is_search()) : ?>

	<header class="page-header">
		<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'xeory-base' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	</header><!-- .page-header -->

	<?php endif;

	get_template_part( 'template-parts/content', 'none' ); ?>

	<?php endif; ?>

	<?php do_action( 'xeory_append_site-main' ); ?>

	</main><!-- #main -->

	<?php do_action( 'xeory_append_content-area' ); ?>

</div><!-- #primary -->